FROM python:3.9.6-slim


ARG BUILD_DEPS="curl"
RUN set -ex && \
    apt-get update && apt-get -y --no-install-recommends install $BUILD_DEPS && \ 
    rm -rf /var/lib/apt/lists/* && \
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python - && \
    . /root/.poetry/env && \
    poetry config virtualenvs.create false && \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false $BUILD_DEPS

COPY poetry.lock pyproject.toml /app/
WORKDIR /app

RUN /root/.poetry/bin/poetry install --no-root --no-interaction && \
    mkdir -p /app/super_recipes/static/images
COPY . /app/

EXPOSE 8000
ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "super_recipes:app"]
