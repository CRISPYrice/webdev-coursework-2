from . import db, app
from .models import User, Role
from flask_security import SQLAlchemyUserDatastore, Security
from flask_security.utils import hash_password

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


@app.before_first_request
def create_user() -> None:
    if not user_datastore.find_user(username="root"):
        user_datastore.create_user(username="root",
                                   email="root@example.com",
                                   password=hash_password("d7e4z4A9"))
        db.session.commit()
