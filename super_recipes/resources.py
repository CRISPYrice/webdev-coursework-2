from sqlalchemy.sql.functions import func
from . import api
from . import db
from typing import Any
from .models import Recipe, Ingredient, Unit, RecipeIngredient
from flask import request
from flask_restful import Resource
import json


class AllRecipesResource(Resource):
    def get(self) -> list[int]:
        return [v.id for v in db.session.query(Recipe.id).distinct()]


class AllIngredientsResource(Resource):
    def get(self) -> list[int]:
        return [i.to_dict() for i in db.session.query(Ingredient).distinct()]


class AllUnitsResource(Resource):
    def get(self) -> list[int]:
        return [u.to_dict() for u in db.session.query(Unit).distinct()]


class RecipeResource(Resource):
    def get(self, recipe_id: str) -> dict:
        if recipe := Recipe.query.get(recipe_id):
            return recipe.to_dict()
        return {}


class RandomRecipeResource(Resource):
    def get(self) -> str:
        if recipe := Recipe.query.order_by(func.random()).first():
            return recipe.to_dict()
        return {}


class IngredientResource(Resource):
    def get(self, ing_id: str) -> dict:
        if ingredient := Ingredient.query.get(ing_id):
            return ingredient.to_dict()
        return {}


class UnitResource(Resource):
    def get(self, unit_id: str) -> dict:
        if unit := Unit.query.get(unit_id):
            return unit.to_dict()
        return {}


class RecipeIngredientResource(Resource):
    def put(self, recipe_id: str) -> dict:
        # Is a put, so any existing data is removed
        for ri in RecipeIngredient.query.filter_by(recipe_id=recipe_id):
            db.session.delete(ri)
        db.session.commit()
        return_items = []
        data = request.get_json()
        for data_item in data:
            ri = RecipeIngredient()
            ri.recipe_id = recipe_id
            ri.ingredient_id = data_item["ingredient_id"]
            ri.unit_id = data_item["unit_id"]
            ri.ingredient_amount = data_item["ingredient_amount"]
            db.session.add(ri)
            db.session.commit()
            return_items.append(ri.to_dict())
        return json.dumps(return_items)


class SearchFilterResource(Resource):
    def get(self) -> list[dict[str, Any]]:
        q = request.args["q"]
        results = []
        results += [{"type": "recipe", "id": recipe.id, "text": recipe.name}
                    for recipe in Recipe.query.all()
                    if q.lower() in recipe.name.lower()]
        results += [{"type": "ingredient",
                     "id": ingredient.id, "text": ingredient.name}
                    for ingredient in Ingredient.query.all()
                    if q.lower() in ingredient.name.lower()]
        if len(results) > 10:
            results = results[:10]
        return results


api.add_resource(AllRecipesResource, "/api/recipes")
api.add_resource(AllIngredientsResource, "/api/ingredients")
api.add_resource(AllUnitsResource, "/api/units")
api.add_resource(RecipeResource, "/api/recipes/<string:recipe_id>")
api.add_resource(RandomRecipeResource, "/api/recipes/random")
api.add_resource(IngredientResource, "/api/ingredients/<string:ing_id>")
api.add_resource(UnitResource, "/api/units/<string:unit_id>")
api.add_resource(RecipeIngredientResource,
                 "/api/recipe-ingredients/<string:recipe_id>")
api.add_resource(SearchFilterResource, "/api/search/filter")
