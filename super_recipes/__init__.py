from . import config
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Api
from flaskext.markdown import Markdown

# Main flask application
app = Flask(__name__)
app.config.from_object(config)
# Application database
db = SQLAlchemy(app)
migrate = Migrate(app, db)
# Flask API
api = Api(app)
# Markdown
Markdown(app)

from . import views, models, resources, security  # noqa
