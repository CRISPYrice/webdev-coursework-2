from . import db
from typing import Any
from flask_security.models import fsqla_v2 as fsqla


class Unit(db.Model):
    """A unit of measurement (grams, millilitres etc.)"""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(16))
    short_name = db.Column(db.String(4))
    recipe_ingredients = db.relationship(
        "RecipeIngredient", backref="unit", lazy="dynamic")

    def to_dict(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "name": self.name,
            "short_name": self.short_name
        }


class Ingredient(db.Model):
    """An ingredient, such as 'apple'"""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description = db.Column(db.String(1024))
    img_url = db.Column(db.String(64))
    recipe_ingredients = db.relationship(
        "RecipeIngredient", backref="ingredient", lazy="dynamic")

    def to_dict(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "image": self.img_url
        }


class Recipe(db.Model):
    """
    A recipe containing all the data to be presented to the user regarding
    how to produce the end product
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description = db.Column(db.String(999))
    serves = db.Column(db.Integer)
    cooking_time = db.Column(db.Integer)
    method = db.Column(db.String(9999))
    img_url = db.Column(db.String(64))
    recipe_ingredients = db.relationship(
        "RecipeIngredient", backref="recipe", lazy="dynamic")

    def to_dict(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "serves": self.serves,
            "cooking_time": self.cooking_time,
            "method": self.method,
            "image": self.img_url,
            "ingredients": [ing.to_dict()
                            for ing in self.recipe_ingredients]
        }


class RecipeIngredient(db.Model):
    """
    Composite relation representing a single ingredient in a recipe and the
    quantity needed
    """
    recipe_id = db.Column(db.Integer, db.ForeignKey(
        "recipe.id"), primary_key=True)
    ingredient_id = db.Column(db.Integer, db.ForeignKey(
        "ingredient.id"), primary_key=True)
    unit_id = db.Column(db.Integer, db.ForeignKey("unit.id"))
    ingredient_amount = db.Column(db.Float)

    def to_dict(self) -> dict[str, Any]:
        return {
            "ingredient": self.ingredient.to_dict(),
            "unit": self.unit.to_dict(),
            "amount": self.ingredient_amount
        }


# Flask Security Models
fsqla.FsModels.set_db_info(db)


class User(db.Model, fsqla.FsUserMixin):
    pass


class Role(db.Model, fsqla.FsRoleMixin):
    pass
